package com.sourcedev.joaozao.alphabetamaps;

import com.fuzzylite.Engine;
import com.fuzzylite.FuzzyLite;
import com.fuzzylite.Op;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.imex.FldExporter;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

import java.util.ArrayList;

/**
 * Created by joaozao on 03/12/16.
 */

public class FuzzyX {


    private ArrayList<Float> inputList =  new ArrayList<>();
    private ArrayList<Float> inputList1 =  new ArrayList<>();
    private ArrayList<Float> outputList =  new ArrayList<>();

    public FuzzyX() {
    }

    public void airConditionerFuzzy() {
        Engine engine = new Engine();
        engine.setName("AirConditioner");

        InputVariable inputVariable1 = new InputVariable();
        inputVariable1.setEnabled(true);
        inputVariable1.setName("RoomTemperature");
        inputVariable1.setRange(0, 40);
        inputVariable1.addTerm(new Triangle("VERYCOLD", 0, 0, 10));
        inputVariable1.addTerm(new Triangle("COLD", 0, 10, 20));
        inputVariable1.addTerm(new Triangle("WARM", 10, 20, 30));
        inputVariable1.addTerm(new Triangle("HOT", 20, 30, 40));
        inputVariable1.addTerm(new Triangle("VERYHOT", 30, 40, 40));
        engine.addInputVariable(inputVariable1);


        InputVariable inputVariable2 = new InputVariable();
        inputVariable2.setEnabled(true);
        inputVariable2.setName("Target");
        inputVariable2.setRange(0, 40);
        inputVariable2.addTerm(new Triangle("VERYCOLD", 0, 0, 10));
        inputVariable2.addTerm(new Triangle("COLD", 0, 10, 20));
        inputVariable2.addTerm(new Triangle("WARM", 10, 20, 30));
        inputVariable2.addTerm(new Triangle("HOT", 20, 30, 40));
        inputVariable2.addTerm(new Triangle("VERYHOT", 30, 40, 40));
        engine.addInputVariable(inputVariable2);

        OutputVariable outputVariable = new OutputVariable();
        outputVariable.setEnabled(true);
        outputVariable.setName("Command");
        outputVariable.setRange(0.000, 1.000);
        outputVariable.fuzzyOutput().setAccumulation(new Maximum());
        outputVariable.setDefuzzifier(new Centroid(200));
        outputVariable.setDefaultValue(Double.NaN);
        outputVariable.setLockPreviousOutputValue(false);
        outputVariable.setLockOutputValueInRange(false);
        outputVariable.addTerm(new Triangle("HEAT", 0.000, 0.250, 0.500));
        outputVariable.addTerm(new Triangle("NOCHANGE", 0.250, 0.500, 0.750));
        outputVariable.addTerm(new Triangle("COOL", 0.500, 0.750, 1.000));
        engine.addOutputVariable(outputVariable);

        //IF temperature=(Cold OR Very_Cold) AND target=Warm THEN  Action HEAT
        //IF temperature=(Hot OR Very_Hot) AND target=Warm THEN Action COOL
        //IF (temperature=Warm) AND (target=Warm) THEN Action NO_CHANGE

        RuleBlock ruleBlock = new RuleBlock();
        ruleBlock.setEnabled(true);
        ruleBlock.setName("");
        ruleBlock.setConjunction(null);
        ruleBlock.setDisjunction(null);
        ruleBlock.setActivation(new Minimum());
        ruleBlock.addRule(Rule.parse("if (RoomTemperature is COLD or RoomTemperature is VERYCOLD) and Target is WARM then Command is HEAT", engine));
        ruleBlock.addRule(Rule.parse("if (RoomTemperature is HOT or RoomTemperature is VERYHOT) and Target is WARM then Command is COOL", engine));
        ruleBlock.addRule(Rule.parse("if RoomTemperature is WARM and Target is WARM then Command is NOCHANGE", engine));
        engine.addRuleBlock(ruleBlock);

        engine.configure("Minimum", "Maximum", "Minimum", "Maximum", "Centroid");

        StringBuilder status = new StringBuilder();
        if (!engine.isReady(status)) {
            throw new RuntimeException("Engine not ready. "
                    + "The following errors were encountered:\n" + status.toString());
        }

        for (int i = 0; i < 100; ++i) {
            double input = inputVariable1.getMinimum() + i * (inputVariable1.range() / 100);
            inputVariable1.setInputValue(input);
            double input1 = inputVariable2.getMinimum() + i * (inputVariable2.range() / 100);
            inputVariable2.setInputValue(input1);
            engine.process();
            FuzzyLite.logger().info(String.format(
                    "Ambient.input1 = %s Ambient.input2 = %s-> Power.output = %s",
                    Op.str(input),Op.str(input1), Op.str(outputVariable.getOutputValue())));
            inputList.add((float) input);
            inputList1.add((float) input1);
            outputList.add((float) outputVariable.getOutputValue());
        }

        FuzzyLite.logger().info(new FldExporter().toString(engine));
    }

    public void fuzzySimpleDimmer() {
        Engine engine = new Engine();
        engine.setName("simple-dimmer");

        InputVariable ambient = new InputVariable();
        ambient.setName("Ambient");
        ambient.setRange(0.000, 1.000);
        ambient.addTerm(new Triangle("DARK", 0.000, 0.250, 0.500));
        ambient.addTerm(new Triangle("MEDIUM", 0.250, 0.500, 0.750));
        ambient.addTerm(new Triangle("BRIGHT", 0.500, 0.750, 1.000));
        engine.addInputVariable(ambient);

        OutputVariable power = new OutputVariable();
        power.setName("Power");
        power.setRange(0.000, 1.000);
        power.setDefaultValue(Double.NaN);
        power.addTerm(new Triangle("LOW", 0.000, 0.250, 0.500));
        power.addTerm(new Triangle("MEDIUM", 0.250, 0.500, 0.750));
        power.addTerm(new Triangle("HIGH", 0.500, 0.750, 1.000));
        engine.addOutputVariable(power);

        RuleBlock ruleBlock = new RuleBlock();
        ruleBlock.addRule(Rule.parse("if Ambient is DARK then Power is HIGH", engine));
        ruleBlock.addRule(Rule.parse("if Ambient is MEDIUM then Power is MEDIUM", engine));
        ruleBlock.addRule(Rule.parse("if Ambient is BRIGHT then Power is LOW", engine));
        engine.addRuleBlock(ruleBlock);

        engine.configure("", "", "Minimum", "Maximum", "Centroid");

        StringBuilder status = new StringBuilder();
        if (!engine.isReady(status)) {
            throw new RuntimeException("Engine not ready. "
                    + "The following errors were encountered:\n" + status.toString());
        }

        for (int i = 0; i < 1000; ++i) {
            double light = ambient.getMinimum() + i * (ambient.range() / 1000);
            ambient.setInputValue(light);

            engine.process();
            FuzzyLite.logger().info(String.format(
                    "Ambient.input = %s -> Power.output = %s",
                    Op.str(light), Op.str(power.getOutputValue())));
            inputList.add((float) light);
            outputList.add((float) power.getOutputValue());
        }
    }

    public ArrayList<Float> getInputList() {
        return inputList;
    }
    public ArrayList<Float> getInputList1() {
        return inputList1;
    }

    public ArrayList<Float> getOutputList() {
        return outputList;
    }


}
