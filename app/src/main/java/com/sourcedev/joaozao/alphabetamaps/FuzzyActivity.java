package com.sourcedev.joaozao.alphabetamaps;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;

public class FuzzyActivity extends AppCompatActivity {

    private LineChartView chart;
    private LineChartView chartt;
    private LineChartData data;
    private int numberOfLines = 3;
    private int maxNumberOfLines = 4;

    private int numberOfPoints = 100;

    float[][] randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];
    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLines = true;
    private boolean hasPoints = true;
    private ValueShape shape = ValueShape.CIRCLE;
    private boolean isFilled = false;
    private boolean hasLabels = false;
    private boolean isCubic = false;
    private boolean hasLabelForSelected = false;
    private boolean pointsHaveDifferentColor;
    private FuzzyX fuzzyX;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuzzy);

        fuzzyX = new FuzzyX();
        //fuzzyX.fuzzySimpleDimmer();
        fuzzyX.airConditionerFuzzy();

        chart = (LineChartView) findViewById(R.id.chart);
        chart.setOnValueTouchListener(new ValueTouchListener());
        chart.setVisibility(View.VISIBLE);
        chart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(FuzzyActivity.this)
                        .title("Fuzzy Memberships")
                        .content("This graphic shows the input memeberships of the fuzzy, the purple plot is the Room Temperature and the green is the Target Temperature")
                        .positiveText("OK")
                        .show();
            }
        });

        chartt = (LineChartView) findViewById(R.id.chart1);
        chartt.setOnValueTouchListener(new ValueTouchListener());
        chartt.setVisibility(View.VISIBLE);
        chartt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        // Generate some random values.
        //generateValues();

        //generateData();
        generateFuzzyData();
        //simpleDimmerFuzzy = new SimpleDimmerFuzzy();
        //simpleDimmerFuzzy.run();
        //generateFuzzyData1();

        // Disable viewport recalculations, see toggleCubic() method for more info.
        chart.setViewportCalculationEnabled(false);


        /*TableView<String[]> tableView = (TableView<String[]>) findViewById(R.id.tableView);
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, dataaa));
        SimpleTableHeaderAdapter headerAdapter = new SimpleTableHeaderAdapter(this, "RoomTemperature", "Target", "Command");
        tableView.setHeaderAdapter(headerAdapter);*/

        resetViewport();

    }

    private void generateFuzzyData() {

        List<Line> lines = new ArrayList<Line>();
        List<Line> lines1 = new ArrayList<Line>();
        for (int i = 0; i < numberOfLines; ++i) {

            List<PointValue> values = new ArrayList<PointValue>();
            List<PointValue> values1 = new ArrayList<PointValue>();
            Line line = new Line();
            Line line1 = new Line();
            switch (i) {
                case 0:
                    resultFuzzy(values1);
                    line1 = new Line(values1);
                    break;
                case 1:
                    memberFuzzy(values);
                    line = new Line(values);
                    break;
                case 2:
                    member1Fuzzy(values);
                    line = new Line(values);
                    break;
                default:
                    line = new Line(values);
                    break;
            }

            if (i == 0) {
                line1.setColor(ChartUtils.COLORS[i]);
                line1.setShape(shape);
                line1.setCubic(isCubic);
                line1.setFilled(isFilled);
                line1.setHasLabels(hasLabels);
                line1.setHasLabelsOnlyForSelected(hasLabelForSelected);
                line1.setHasLines(hasLines);
                line1.setHasPoints(hasPoints);
                if (pointsHaveDifferentColor){
                    line1.setPointColor(ChartUtils.COLORS[(i + 1) % ChartUtils.COLORS.length]);
                }
                lines1.add(line1);
                data = new LineChartData(lines1);

                if (hasAxes) {
                    Axis axisX = new Axis();
                    Axis axisY = new Axis().setHasLines(true);
                    if (hasAxesNames) {
                        axisX.setName("Axis X");
                        axisY.setName("Axis Y");
                    }
                    data.setAxisXBottom(axisX);
                    data.setAxisYLeft(axisY);
                } else {
                    data.setAxisXBottom(null);
                    data.setAxisYLeft(null);
                }

                data.setBaseValue(Float.NEGATIVE_INFINITY);
                chartt.setLineChartData(data);
            } else {
                line.setColor(ChartUtils.COLORS[i]);
                line.setShape(shape);
                line.setCubic(isCubic);
                line.setFilled(isFilled);
                line.setHasLabels(hasLabels);
                line.setHasLabelsOnlyForSelected(hasLabelForSelected);
                line.setHasLines(hasLines);
                line.setHasPoints(hasPoints);
                if (pointsHaveDifferentColor){
                    line.setPointColor(ChartUtils.COLORS[(i + 1) % ChartUtils.COLORS.length]);
                }
                lines.add(line);
                data = new LineChartData(lines);

                if (hasAxes) {
                    Axis axisX = new Axis();
                    Axis axisY = new Axis().setHasLines(true);
                    if (hasAxesNames) {
                        axisX.setName("Axis X");
                        axisY.setName("Axis Y");
                    }
                    data.setAxisXBottom(axisX);
                    data.setAxisYLeft(axisY);
                } else {
                    data.setAxisXBottom(null);
                    data.setAxisYLeft(null);
                }

                data.setBaseValue(Float.NEGATIVE_INFINITY);
                chart.setLineChartData(data);
            }
        }

    }

    private void resultFuzzy(List<PointValue> pValues) {
        for (int j = 0; j < fuzzyX.getInputList().size(); ++j) {
            pValues.add(new PointValue(fuzzyX.getInputList().get(j), fuzzyX.getOutputList().get(j)));
            Log.d("_fuzzy12", "input : " + fuzzyX.getInputList().get(j) + "   output : " + fuzzyX.getOutputList().get(j));
        }

       /* String[] stringValues = new String[fuzzyX.getInputList().size()];

        for (int i = 0; i < stringValues.length; i++) {
            stringValues[i] = String.valueOf(fuzzyX.getInputList().get(i));
            dataaa.add(i, stringValues);
            Log.d("_OLAA", "dataa : "+ dataaa.toString());
        }*/

    }

    private void memberFuzzy(List<PointValue> pValues) {
        ArrayList<Float> yReverse = new ArrayList<>();
        for(double i = 1.0; i>=0; i= i-0.1) {
            yReverse.add((float) i);
        }
        for (int j = 0; j <= 10; ++j) {
            pValues.add(new PointValue(j, (float) j/10));
            Log.d("_fuzzy12", "input membership : " + j + "   output memebership : " +  (float) j/10);
        }
        for (int j = 10; j <= 20; ++j) {
            pValues.add(new PointValue(j, yReverse.get(j-10)));
            Log.d("_fuzzy12", "input membership : " + j + "   output memebership : " +  yReverse.get(j-10));
        }
        for (int j = 20; j <= 30; ++j) {
            pValues.add(new PointValue(j, (float) (j-20)/10));
            //Log.d("_fuzzy12", "input membership : " + j + "   output memebership : " + j/10);
        }
        for (int j = 30; j <= 40; ++j) {
            pValues.add(new PointValue(j, yReverse.get(j-30)));
            //Log.d("_fuzzy12", "input membership : " + j + "   output memebership : " + j/10);
        }
    }

    private void member1Fuzzy(List<PointValue> pValues) {
        ArrayList<Float> yReverse = new ArrayList<>();
        for(double i = 1.0; i>=0; i= i-0.1) {
            yReverse.add((float) i);
        }
        for (int j = 0; j <= 10; ++j) {
            pValues.add(new PointValue(j, yReverse.get(j)));
            Log.d("_fuzzy12", "input membership : " + j + "   output memebership : " +  (float) j/10);
        }
        for (int j = 10; j <= 20; ++j) {
            pValues.add(new PointValue(j, (float) (j-10)/10));
            Log.d("_fuzzy12", "input membership : " + j + "   output memebership : " +  yReverse.get(j-10));
        }
        for (int j = 20; j <= 30; ++j) {
            pValues.add(new PointValue(j, yReverse.get(j-20)));
            //Log.d("_fuzzy12", "input membership : " + j + "   output memebership : " + j/10);
        }
        for (int j = 30; j <= 40; ++j) {
            pValues.add(new PointValue(j, (float)(j-30)/10));
            //Log.d("_fuzzy12", "input membership : " + j + "   output memebership : " + j/10);
        }
    }


    private void reset() {
        numberOfLines = 1;

        hasAxes = true;
        hasAxesNames = true;
        hasLines = true;
        hasPoints = true;
        shape = ValueShape.CIRCLE;
        isFilled = false;
        hasLabels = false;
        isCubic = false;
        hasLabelForSelected = false;
        pointsHaveDifferentColor = false;

        chart.setValueSelectionEnabled(hasLabelForSelected);
        resetViewport();
    }

    private void resetViewport() {
        // Reset viewport height range to (0,100)
        final Viewport v = new Viewport(chart.getMaximumViewport());
        v.bottom = 0;
        v.top = 1;
        v.left = 0;
        //v.right = numberOfPoints - 1;
        v.right = 40;
        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);
    }




    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.fuzzy_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class ValueTouchListener implements LineChartOnValueSelectListener {

        @Override
        public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
            Toast.makeText(FuzzyActivity.this, "Selected: " + value, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }
}
